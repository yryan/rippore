# RIPpore

RIPpore is a python script written to quantify levels of depurination in 28s rRNA from Oxford Nanopore direct RNA sequencing

# Prerequisits
RIPpore requires the following packages:
* pysam
* pandas
* argparse

# Usage

RIPpore by default takes 28s rRNA, sequenced with Oxford Nanopores direct RNA sequencing, mapped to NCBI Reference Sequence: [NR_003287.4](https://www.ncbi.nlm.nih.gov/nuccore/NR_003287), producing a sam file, then indexing the produced samfile. The indexed sam file is used by RIPpore to quantify the counts of each nucleotide at position 4605, the depurinated adenine in the ricin loop of the 28s ribosome.

By default RIPpore takes an sorted and indexed sam file, and prints the proportion of A to T tranposition, and whether it is depurinated or not.

`rippore.py -s sample.sam`

## All Options

| Short hand | Full     | Description     |
| :------------- | :----------: | -----------: |
|  -s | --sam  | Path to indexed sam file (Required) |
| -o   | --output | Output location for CSV detailing counts of each nucleotide at selected position, 4605 by default |
| -b   | --base | Location counting nucleotides at an alternative base. Useful for different refrences or species |
| -r   | --read_list | Produces a text file for each nucleotide, containing the read names for each occurance of that nucleotide. Useful for diagnostic purposes and deeper signal level analysis |
| -a | --accession | Provide accession if NR_003287.4 has not been used for alignment. |

# Raw data

Available at BioProject PRJNA752594

# Citation

Please cite: biorxiv here / published paper when done
