#!/usr/bin/env python3
import pysam
import os
import pandas as pd
import argparse


def stats_4605(input_file, base_p, reads_by_base, output, accession):
    base_p = int(base_p)
    mx = base_p + 1
    mn = base_p -1

    a = 0
    t = 0
    c = 0
    g = 0
    total = 0
    a_list = []
    t_list = []
    c_list = []
    g_list = []

    cols = ["base", "total_reads", "a_count", "t_count", "c_count", "g_count"]

    df = pd.DataFrame(columns=cols)
    samfile = pysam.AlignmentFile(input_file, "rb")

    print("RIPPore running:")

    for pileupcolumn in samfile.pileup(accession, mn, mx, max_depth=10000000):
        if pileupcolumn.pos != base_p:
            continue
        else:
            base = pileupcolumn.pos + 1
            pileupcolumn.set_min_base_quality(4)
            for pileupread in pileupcolumn.pileups:
                total += 1

                if not pileupread.is_del and not pileupread.is_refskip:
                    if pileupread.alignment.query_sequence[pileupread.query_position] == "A":
                        a += 1
                        a_list.append(pileupread.alignment.query_name)
                    elif pileupread.alignment.query_sequence[pileupread.query_position] == "T":
                        t += 1
                        t_list.append(pileupread.alignment.query_name)
                    elif pileupread.alignment.query_sequence[pileupread.query_position] == "C":
                        c += 1
                        c_list.append(pileupread.alignment.query_name)
                    elif pileupread.alignment.query_sequence[pileupread.query_position] == "G":
                        g += 1
                        g_list.append(pileupread.alignment.query_name)

            data = [[base, total, a, t, c, g]]
        df = df.append(pd.DataFrame(data, columns=cols), ignore_index=True)

        if reads_by_base:
            a_txt = open('list_dep_a.txt', 'w')
            a_txt.writelines("%s\n" % read for read in a_list)
            a_txt.close()

            t_txt = open('list_dep_t.txt', 'w')
            t_txt.writelines("%s\n" % read for read in t_list)
            t_txt.close()

            c_txt = open('list_dep_c.txt', 'w')
            c_txt.writelines("%s\n" % read for read in c_list)
            c_txt.close()

            g_txt = open('list_dep_g.txt', 'w')
            g_txt.writelines("%s\n" % read for read in g_list)
            g_txt.close()

    if output:
        df.to_csv(output, sep=",")

    os.chdir("..")
    samfile.close()
    
    dep_level = (t/total)*100
    
    is_dep = False 

    if dep_level > 0.4:
        is_dep = True

    print("Percentage of depurinated reads: %.3f" % dep_level)
    print("Sample is depurinated" if is_dep else "Sample is not depurinated") 



# Argument parser for command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-s", "--sam_file", required=True,
                    help="Please provide the path to your sorted and indexed samfile of mapped ribosomal reads.")
parser.add_argument("-o", "--output", required=False, help="Enter path and filename for csv output")
parser.add_argument("-b", "--base", help="Select Alternate base, be careful not to select out of range of chosen index.", default=4604)
parser.add_argument("-r", "--read_list", help="If selected produces four .txt files, with the read names for each read,"
                                              "for each individual base. Only useful for diagnostic purposes or basecaller training.",
                    action="store_true")
parser.add_argument("-a", "--accession", help="Provide accession if NR_003287.4 has not been used for alignment.",
                    default="NR_003287.4", required=False)

args = parser.parse_args()

stats_4605(vars(args)['sam_file'], vars(args)['base'], vars(args)['read_list'],
           vars(args)['output'], vars(args)["accession"])
